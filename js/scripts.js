/* Global variables */

var n = 1.0

var hitOptions = {
  stroke: true,
  fill: true,
  tolerance: 5,
}

var buttonWidth = 20
var buttonHeight = 60

var rays = []

/* Physics implemetation - refraction, reflection, dispersion */

function getRefractiveIndex(wavelength, material) {

  //BK7 - crown;  SF11 - flint

  if (material == "BK7") {
    B1 = 1.03961212
    B2 = 0.231792344
    B3 = 1.01046945
    C1 = 0.00600069867
    C2 = 0.0200179144
    C3 = 103.560653
  } else if (material == "SF11") {
    B1 = 1.73759695
    B2 = 0.313747346
    B3 = 1.89878101
    C1 = 0.013188707
    C2 = 0.0623068142
    C3 = 155.23629
  }
  return Math.sqrt(1 + B1 * wavelength * wavelength / (wavelength * wavelength - C1) + B2 * wavelength * wavelength / (wavelength * wavelength - C2) + B3 * wavelength * wavelength / (wavelength * wavelength - C3))
}


function refraction(ray, element, node, n1, n2) {
  var incoming = (node - ray.firstSegment.point).normalize()
  var normal = element.getLocationOf(node).normal
  var tangent = -element.getLocationOf(node).tangent
  var sina = -incoming.cross(normal)
  var sinb

  sinb = n1 / n2 * sina

  var cosb
  if (Math.abs(sinb) <= 1) {
    cosb = Math.sqrt(1 - sinb * sinb)
  } else {
    cosb = NaN
    return reflection(ray, element, node)
  }

  cosb = Math.sqrt(1 - sinb * sinb)

  var refracted = normal * Math.sign(incoming.dot(normal)) * cosb - tangent * sinb
  return refracted
}


function reflection(ray, element, node) {
  var incoming = (node - ray.firstSegment.point).normalize()
  var normal = element.getLocationOf(node).normal
  var cosa = -incoming.dot(normal)
  var incomingPart = normal * cosa
  var reflected = incoming + incomingPart * 2
  return reflected
}


function renewRay(ray, elementCount) {
  var previousPoint = ray.segments[elementCount].point
  var previousVector = ray.getPointAt(ray.getOffsetOf(previousPoint) + 0.01) - previousPoint
  ray.removeSegments(elementCount + 1)
  ray.add(previousPoint + previousVector * 300000)
}


var background = new Path.Rectangle({
  point: [0, 0],
  size: [view.size.width, view.size.height],
  fillColor: '#373b41'
})
background.sendToBack()

var mainMenuBg = new Path.Rectangle({
  point: [0, 0],
  size: [40, view.size.height],
  fillColor: "#000",
  opacity: 0.3
})


var grid = new Group()
var gridSize = 50
for (var i = 0; i < view.size.width / gridSize; i++) {
  var line = new Path({
    segments: [[40 + (i + 1) * gridSize, 0], [40 + (i + 1) * gridSize, view.size.width]],
    strokeColor: 'white',
    strokeWidth: 0.5,
    opacity: 0.15
  });
  grid.addChild(line)
}

for (var i = 0; i < view.size.height / gridSize; i++) {
  var line = new Path({
    segments: [[40, (i + 1) * gridSize], [view.size.width, (i + 1) * gridSize]],
    strokeColor: 'white',
    strokeWidth: 0.5,
    opacity: 0.15
  });
  grid.addChild(line)
}

/*
--lightsources
*/

var laserBase = new Path.Star(new Point(30, 350), 12, 25, 14);
var rectangle = new Rectangle(new Point(25, 344), new Size(30, 12))
var cornerSize = new Size(7, 7)
var roundedRectangle = new Path.Rectangle(rectangle, cornerSize)
var laser = laserBase.subtract(roundedRectangle)
laser.fillColor = '#ff7f11'
laser.moveable = true
laser.lightSource = true
laser.lightSourcesCount = 1
laser.solid = true
laser.transformContent = false

turnOff = function(element) {
  var actualIds = []
  for (var i = 1; i <= element.parent.children.length; i++) {
  //   // if (element.parent.children[i]) console.log('petla', element.parent.children[i].id);
  //   // if (element.parent.children[i]) console.log(rays.indexOf(element.parent.children[i]));
    // rays.splice(rays.indexOf(element.parent.children[i]), 1)
    if (element.parent.children[i]) actualIds.push(element.parent.children[i].id)
  }
  // console.log(actualIds);
  for (var i = 0; i < rays.length; i++) {
    // console.log(rays[i]);
    if (actualIds.indexOf(rays[i].id) != -1) {
      rays.splice(i,1)
    }
  }
  element.parent.removeChildren(1)
}

turnOn = function(element, color) {
  // console.log(element.lightSourcesCount);
  var rayPosition = element.position
  if (element.lightSourcesCount > 1) {
    rayPosition += [0, -30]
  }
  for (var i = 0; i < element.lightSourcesCount; i++) {

    var ray = new Path({
      strokeWidth: 2
    })

    ray.add(rayPosition)
    vector = new Point(rayPosition.x + 1, 0) - rayPosition
    vector.angle = element.rotation
    ray.add(rayPosition + vector * 1000)

    if (color == 'red') {
      ray.strokeColor = '#CF3838'
      ray.wavelength = 0.6328
    }

    if (color == 'green') {
      ray.strokeColor = '#92C134'
      ray.wavelength = 0.520
    }

    if (color == 'blue') {
      ray.strokeColor = '#227C7C'
      ray.wavelength = 0.405
    }

    element.parent.addChild(ray)
    rays.push({'ray': ray, 'id': ray.id})
    rayPosition += [0, 15]
  }
}


laser.position += [50, 0]
laser.scale(0.7)

laser.scaleFactor = buttonWidth / laser.bounds.width * 1.3
laser.mainMenuButton = true

laser.scale(laser.scaleFactor)
laser.position = [buttonWidth, buttonHeight * 0.75]


var multiLaser = new Path.Rectangle(new Rectangle(new Point(200, 200), new Size(10, 90)), new Size(2, 2))

var holeHeight = multiLaser.bounds.height / 10
var roundedRectangleMini = new Path.Rectangle(new Rectangle(multiLaser.position + new Point(-2, -holeHeight/2), new Size(20, holeHeight)), new Size(5, 5))
// roundedRectangleMini.fillColor = 'red'
roundedRectangleMini.position += [0, -30]

for (var i = 0; i < 5; i++) {
  multiLaser = multiLaser.subtract(roundedRectangleMini)
  roundedRectangleMini.position += [0, 15]
}

multiLaser.fillColor = '#ff7f11'
multiLaser.moveable = true
multiLaser.lightSource = true
multiLaser.lightSourcesCount = 5
multiLaser.solid = true
multiLaser.scaleFactor = buttonHeight / multiLaser.bounds.height
multiLaser.mainMenuButton = true
multiLaser.scale(multiLaser.scaleFactor)
multiLaser.position = [buttonWidth, buttonHeight * 1.75]

/*
--lenses
*/


var convexLensLeft = new Path.Circle(new Point(-30, 60), 75)
var convexLensRight = new Path.Circle(new Point(90, 60), 75)
var convexLens = convexLensLeft.intersect(convexLensRight)
convexLens.fillColor = '#3bc7e5'
convexLens.refractive = true
convexLens.type = "refractive"
convexLens.material = "SF11"
convexLens.glass = true
convexLens.mainMenuButton = true
convexLens.scaleFactor = buttonWidth / convexLens.bounds.width

convexLens.scale(convexLens.scaleFactor)
convexLens.position = [buttonWidth, buttonHeight * 3.5]

var concaveLensBase = new Path.Rectangle(new Point(10, 130), new Size(40, 90))
var concaveLensLeft = new Path.Circle(new Point(-50, 175), 75)
var concaveLensRight = new Path.Circle(new Point(110, 175), 75)
var concaveLens = concaveLensBase.subtract(concaveLensLeft)
concaveLens = concaveLens.subtract(concaveLensRight)
concaveLens.fillColor = '#3bc7e5'
concaveLens.refractive = true
concaveLens.type = "refractive"
concaveLens.material = "SF11"
concaveLens.glass = true
concaveLens.mainMenuButton = true
concaveLens.scaleFactor = buttonWidth / concaveLens.bounds.width

concaveLens.scale(concaveLens.scaleFactor)
concaveLens.position = [buttonWidth, buttonHeight * 4.75]

var prism = new Path.RegularPolygon({
  center: [30, 280],
  sides: 3,
  radius: 30,
  fillColor: '#3bc7e5',
  material: 'SF11',
  glass: true,
  refractive: true,
  type: "refractive",
  mainMenuButton: true
})

prism.scaleFactor = buttonWidth / prism.bounds.width * 1.4


prism.scale(prism.scaleFactor)
prism.position = [buttonWidth, buttonHeight * 5.75]

var block = new Path.Rectangle(new Point(10, 130), new Size(25, 90))
block.fillColor = '#3bc7e5'
block.refractive = true
block.type = "refractive"
block.material = "SF11"
block.glass = true
block.mainMenuButton = true
block.scaleFactor = buttonWidth * 0.7 / block.bounds.width

block.scale(block.scaleFactor)

block.position = [buttonWidth, buttonHeight * 6.75]


/*
--mirrors
*/


var mirror = new Path({
  strokeWidth: 7,
  strokeColor: '#f1e7c1',
})

mirror.add(new Point(100, 100));
mirror.add(new Point(100, 150));

mirror.glass = true
mirror.reflective = true
mirror.type = "reflective"
mirror.mainMenuButton = true
mirror.scaleFactor = buttonHeight * 0.8 / mirror.bounds.height
mirror.scale(mirror.scaleFactor)
mirror.position = [buttonWidth, buttonHeight * 8.5]
mirror.noHorizontalResize = true


var convexMirror = new Path.Arc({
    from: [20, 480],
    through: [35, 520],
    to: [20, 560],
    strokeColor: '#f1e7c1',
    strokeWidth: 7,
    moveable: true,
    reflective: true,
    glass: true,
    type: "reflective",
    mainMenuButton: true,
    scaleFactor: buttonHeight * 0.5 / mirror.bounds.height,
})

convexMirror.scale(convexMirror.scaleFactor)
convexMirror.position = [buttonWidth, buttonHeight * 9.75]


/*
--buttons
*/

var gridButton = new Group()

var buttonBackground = new Path.Rectangle({
  point: [100, 100],
  size: [21, 21],
  fillColor: 'black',
  opacity: 0
})

gridButton.addChild(buttonBackground)
var buttonLine

buttonLine = new Path({
  segments: [[106, 100], [106, 121]],
  strokeColor: 'white',
  strokeWidth: 2,
});

gridButton.addChild(buttonLine)

buttonLine = new Path({
  segments: [[115, 100], [115, 121]],
  strokeColor: 'white',
  strokeWidth: 2,
});
gridButton.addChild(buttonLine)

buttonLine = new Path({
  segments: [[100, 106], [121, 106]],
  strokeColor: 'white',
  strokeWidth: 2,
});
gridButton.addChild(buttonLine)

buttonLine = new Path({
  segments: [[100, 115], [121, 115]],
  strokeColor: 'white',
  strokeWidth: 2,
});
gridButton.addChild(buttonLine)

gridButton.position = [buttonWidth, buttonHeight * 11.5]
gridButton.opacity = 0.5

gridButton.onMouseEnter = function(event) {
  this.opacity = 1
}

gridButton.onMouseLeave = function(event) {
  this.opacity = 0.5
}

gridButton.onMouseDown = function(event) {
  grid.visible = !grid.visible
}


var instructionButton = new Group()

var instructionBackground = new Path.Rectangle({
  size: [30, 30],
  position: [buttonWidth, buttonHeight * 12.35],
  fillColor: 'white',
  opacity: 0
})

instructionButton.addChild(instructionBackground)

var instructionText = new PointText({
  point: [buttonWidth, buttonHeight * 12.5],
  justification: 'center',
  fillColor: 'white',
  fontFamily: 'monospace',
  fontSize: 25,
  content: '?',
});

instructionButton.addChild(instructionText)

instructionButton.opacity = 0.5

instructionButton.onMouseEnter = function(event) {
  this.opacity = 1
}

instructionButton.onMouseLeave = function(event) {
  this.opacity = 0.5
}

instructionButton.onMouseDown = function(event) {
  window.open('https://gitlab.com/lukasz-tomala/simple-optics#readme')
}

var activeElements = []
var elementsToRaytracing = []

function toggleVisibility(element) {
  if (element.visible) {
    element.visible = false;
  } else {
    element.visible = true;
  }
}


function rotate(element, direction) {
  if (!element.customRotation) element.customRotation = 0
  var step = 15

  if (direction == 'left') {
    step = -step
  }
  element.rotate(step)
  element.customRotation += step

  if (element.parent.lightSource) {
    var group = element.parent
    for (i = 1; i < group.children.length; i++) {
      group.children[i].rotate(step, element.position)
    }
  }

}

var elementMenu = new Group()

function showElementMenu(element) {

  var menuWidth = 75
  var menuHeight = 15

  var oneFifth = new Size(menuWidth / 5, menuHeight)
  var oneFourth = new Size(menuWidth / 4, menuHeight)
  var oneThird = new Size(menuWidth / 3, menuHeight)
  var oneHalf = new Size(menuWidth / 2, menuHeight)

  elementMenu.removeChildren()

  menuElementPosition = element.position - new Point(menuWidth / 2, 0.5 * Math.max(element.bounds.height, element.bounds.width) + 2 * menuHeight)

  if (element.lightSource) {

    var redSwitch = new Path.Rectangle(menuElementPosition, oneFifth)
    redSwitch.fillColor = '#CF3838'
    redSwitch.onClick = function(event) {
      turnOff(element)
      turnOn(element, 'red')
    }
    elementMenu.addChild(redSwitch)

    menuElementPosition += new Point(oneFifth.width, 0)

    var greenSwitch = new Path.Rectangle(menuElementPosition, oneFifth)
    greenSwitch.fillColor = '#92C134'
    greenSwitch.onClick = function(event) {
      turnOff(element)
      turnOn(element, 'green')
    }
    elementMenu.addChild(greenSwitch)

    menuElementPosition += new Point(oneFifth.width, 0)

    var blueSwitch = new Path.Rectangle(menuElementPosition, oneFifth)
    blueSwitch.fillColor = '#227C7C'
    blueSwitch.onClick = function(event) {
      turnOff(element)
      turnOn(element, 'blue')
    }
    elementMenu.addChild(blueSwitch)

    menuElementPosition += new Point(oneFifth.width, 0)

    var whiteSwitch = new Path.Rectangle(menuElementPosition, oneFifth)
    // whiteSwitch.fillColor = new Color(255, 255, 255, 0.7)
    whiteSwitch.fillColor = {
        gradient: {
            stops: ['#CF3838', '#92C134', '#227C7C']
        },
        origin: menuElementPosition,
        destination: menuElementPosition + [0, oneFifth.height]
    }
    whiteSwitch.onClick = function(event) {
      turnOff(element)
      turnOn(element, 'red')
      turnOn(element, 'green')
      turnOn(element, 'blue')
    }
    elementMenu.addChild(whiteSwitch)

    menuElementPosition += new Point(oneFifth.width, 0)

    var noneSwitch = new Path.Rectangle(menuElementPosition, oneFifth)
    noneSwitch.fillColor = new Color(0, 0, 0, 0.7)
    noneSwitch.onClick = function(event) {
      turnOff(element)
    }
    elementMenu.addChild(noneSwitch)

  }

  if (element.glass && !element.reflective) {

    var flintSwitchButton = new Path.Rectangle(menuElementPosition, oneHalf)
    flintSwitchButton.fillColor = '#3bc7e5'

    var flintSwitchText = new PointText({
      point: menuElementPosition + [oneHalf.width / 2, oneHalf.height - 4],
      justification: 'center',
      fillColor: 'black',
      fontFamily: 'monospace',
      fontSize: 10,
      content: 'FLINT',
    });

    var flintSwitch = new Group({children: [flintSwitchButton, flintSwitchText]})

    flintSwitch.onClick = function(event) {
      element.material = 'SF11'
      element.fillColor = '#3bc7e5'
    }

    elementMenu.addChild(flintSwitch)

    menuElementPosition += new Point(oneHalf.width, 0)

    var crownSwitchButton = new Path.Rectangle(menuElementPosition, oneHalf)
    crownSwitchButton.fillColor = '#237789'

    var crownSwitchText = new PointText({
      point: menuElementPosition + [oneHalf.width / 2, oneHalf.height - 4],
      justification: 'center',
      fillColor: 'white',
      fontFamily: 'monospace',
      fontSize: 10,
      content: 'CROWN',
    });

    var crownSwitch = new Group({children: [crownSwitchButton, crownSwitchText]})

    crownSwitch.onClick = function(event) {
      element.material = 'BK7'
      element.fillColor = '#237789'
    }

    elementMenu.addChild(crownSwitch)
  }

  menuElementPosition = element.position + new Point(-menuWidth / 2, 0.5 * Math.max(element.bounds.height, element.bounds.width) + menuHeight)

  var rotateLeftButton = new Path.Rectangle(menuElementPosition, oneFourth)
  rotateLeftButton.fillColor = '#f1e7c1'

  var rotateLeftText = new PointText({
    point: menuElementPosition + [oneFourth.width / 2, oneFourth.height - 1],
    justification: 'center',
    fillColor: 'black',
    fontFamily: 'monospace',
    fontSize: 22,
    content: '↶'
  });

  var rotateLeft = new Group({children: [rotateLeftButton, rotateLeftText]})

  rotateLeft.onClick = function(event) {
    rotate(element, 'left')
  }

  elementMenu.addChild(rotateLeft)


  menuElementPosition += new Point(oneFourth.width * 1, 0)

  if (!element.lightSource) {
    var verticalResizeButton = new Path.Rectangle(menuElementPosition, oneFourth)
    verticalResizeButton.fillColor = '#f1e7c1'
    var verticalResizeText = new PointText({
      point: menuElementPosition + [oneFourth.width / 2, oneFourth.height - 1],
      justification: 'center',
      fillColor: 'black',
      fontFamily: 'monospace',
      fontSize: 22,
      content: '↕'
    });

    var verticalResize = new Group({children: [verticalResizeButton, verticalResizeText]})

    verticalResize.onClick = function(event) {
      if (element.customRotation) element.rotate(-element.customRotation);
      element.scale(1, 1.1);
      if (element.customRotation) element.rotate(element.customRotation);
    }

    elementMenu.addChild(verticalResize)
  }


  menuElementPosition += new Point(oneFourth.width * 1, 0)

  if (!element.noHorizontalResize && !element.lightSource) {
    var horizontalResizeButton = new Path.Rectangle(menuElementPosition, oneFourth)
    horizontalResizeButton.fillColor = '#f1e7c1'
    var horizontalResizeText = new PointText({
      point: menuElementPosition + [oneFourth.width / 2, oneFourth.height - 1],
      justification: 'center',
      fillColor: 'black',
      fontFamily: 'monospace',
      fontSize: 22,
      content: '↔'
    });

    var horizontalResize = new Group({children: [horizontalResizeButton, horizontalResizeText]})

    horizontalResize.onClick = function(event) {
      if (element.customRotation) element.rotate(-element.customRotation);
      element.scale(1.1, 1);
      if (element.customRotation) element.rotate(element.customRotation);
    }

    elementMenu.addChild(horizontalResize)
  }
  menuElementPosition += new Point(oneFourth.width * 1, 0)

  var rotateRightButton = new Path.Rectangle(menuElementPosition, oneFourth)
  rotateRightButton.fillColor = '#f1e7c1'

  var rotateRightText = new PointText({
    point: menuElementPosition + [oneFourth.width / 2, oneFourth.height - 1],
    justification: 'center',
    fillColor: 'black',
    fontFamily: 'monospace',
    fontSize: 22,
    content: '↷',
  });

  var rotateRight = new Group({children: [rotateRightButton, rotateRightText]})

  rotateRight.onClick = function(event) {
    rotate(element, 'right')
  }
  elementMenu.addChild(rotateRight)

  elementMenu.visible = true

  childrenCount = elementMenu.children.length

  for (i = 0; i < childrenCount; i++) {
    elementMenu.children[i].opacity = 0.6

    elementMenu.children[i].onMouseEnter = function(event) {
      this.opacity = 1
    }

    elementMenu.children[i].onMouseLeave = function(event) {
      this.opacity = 0.6
    }
  }

}

function rayTracing() {
  if (rays.length) {
    for (var i = 0; i < rays.length; i++) {

      var ray = rays[i].ray
      var intersections = [];
      var node = 0;
      renewRay(ray, 0);
      if (lastPart) {
        lastPart.remove();
      }
      var lastPart = new Path([ray.segments.slice(-2)[0].point, ray.lastSegment.point]);
      for (var j = 0; j < elementsToRaytracing.length; j++) {
        element = elementsToRaytracing[j];
        intersections = intersections.concat(element.getCrossings(lastPart));
      }
      intersections.forEach(function(item, index) {
        item.rayOffset = ray.getOffsetOf(item.point)
      })
      intersections.sort(function(obj1, obj2) {
        return obj1.rayOffset - obj2.rayOffset;
      })
      do {
        if (intersections.length) {
          node = intersections[0];
          currentElement = node.curve.path;

          if (currentElement.reflective) {
            ray.insert(ray.segments.length - 1, new Point(node.point));
            ray.lastSegment.point = node.point + reflection(lastPart, currentElement, node.point) * 3000
          } else if (currentElement.refractive) {
            ray.insert(ray.segments.length - 1, new Point(node.point));
            var n1, n2
            var testPoint = ray.getPointAt(ray.getOffsetOf(node.point) + .0001)
            if (currentElement.contains(testPoint)) {
              n1 = n;
              n2 = getRefractiveIndex(ray.wavelength, currentElement.material)
            } else {
              n1 = getRefractiveIndex(ray.wavelength, currentElement.material)
              n2 = n;
            }
            ray.lastSegment.point = node.point + refraction(lastPart, currentElement, node.point, n1, n2) * 3000
          } else {
            return
          }
        }
        if (lastPart) {
          lastPart.remove();
        }
        var lastPart = new Path([ray.getPointAt(ray.getOffsetOf(ray.segments.slice(-2)[0].point) + .0001), ray.lastSegment.point]);
        intersections = [];
        for (var j = 0; j < elementsToRaytracing.length; j++) {
          element = elementsToRaytracing[j];
          intersections = intersections.concat(element.getCrossings(lastPart));
        }
        intersections.forEach(function(item, index) {
          item.rayOffset = ray.getOffsetOf(item.point)
        })
        intersections.sort(function(obj1, obj2) {
          return obj1.rayOffset - obj2.rayOffset;
        })
      } while (intersections.length)
    }
  }
}

var path

function onMouseDown(event) {
  path = null
  var previousPath
  var hitResult = project.hitTest(event.point, hitOptions)
  if (hitResult) {
    path = hitResult.item
    if (path.moveable) {
      previousPath = path
    }
    if (path.mainMenuButton) {
      var copy = path.clone()
      event.target = copy
      copy.mainMenuButton = false
      copy.moveable = true
      var extraProperties = ['glass', 'type', 'refractive', 'reflective', 'material', 'lightSource', 'lightSourcesCount', 'noHorizontalResize']
      for (var i = 0; i < extraProperties.length; i++) {
        var property = extraProperties[i]
        if (path[property]) {
          copy[property] = path[property]
        }
        if (path.lightSource) {
          new Group({
            children: [copy],
            lightSource: true,
          })
        }
      }
      copy.scale(1 / path.scaleFactor)
      if (!path.lightSource) {
        copy.scale(1.5)
        elementsToRaytracing.push(copy)
      }
      activeElements.push(copy)
      path = copy
    }
  }
  path.onDoubleClick = function(event) {
    if (path.moveable) {
      showElementMenu(path)
    }
  }
  if (!path.isDescendant(elementMenu) && elementMenu.visible) {
    elementMenu.visible = false
  }
}

function onMouseUp(event) {
  rayTracing()
  if (path) {
    if (path.moveable) {
      if (path.isInside(mainMenuBg.bounds) || path.intersects(mainMenuBg)) {
        if (path.parent.lightSource) {
          var group = path.parent
          for (i = 1; i < group.children.length; i++) {
            group.children[i].remove()
          }
        }
        path.remove()
      }
    }
  }
}


function onMouseDrag(event) {
  if (path) {
    if (path.moveable) {
      path.position += event.delta
      if (path.parent.lightSource) {
        var group = path.parent
        for (i = 1; i < group.children.length; i++) {
          group.children[i].position += event.delta
        }
      }
      rayTracing()
      for (var i = 0; i < activeElements.length; i++) {
        if (i != activeElements.indexOf(path)) {
          if (path.intersects(activeElements[i])) {
            path.position -= event.delta
            if (path.parent.lightSource) {
              var group = path.parent
              for (i = 1; i < group.children.length; i++) {
                group.children[i].position -= event.delta
              }
            }
          }
        }
      }
    }
  }
}
