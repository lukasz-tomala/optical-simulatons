# Simple Optics
Author: Łukasz Tomala (lukasz124@gmail.com)

Live app: [lukasz-tomala.gitlab.io/simple-optics](http://lukasz-tomala.gitlab.io/simple-optics)

Simple interactive optical simulations for school students. Use lenses, morrors and lasers to simulate physical phenomena.


Implemented physics:
* dispersion - Sellmeier equation
* refraction - Snells law
* reflection from mirrors
* internal reflection

## Usage

Drag and drop element from menu to use it. **Double click on element to see menu**. Elements can be rotated and resized. There are two types of glass (flint and crown) and three types of lasers.

![](instructions/manipulating.gif)

![](instructions/lightsource.gif)

## Examples

### Fiber

![](instructions/fiber.gif)

## Credits
* [Piotr Nieżurawski](http://pionie.pl/) - vector calculations for refraction and reflection
* [Paper.js](http://paperjs.org) — The Swiss Army Knife of Vector Graphics Scripting
